<?php

namespace designerei\ContaoLogoBundle\Controller\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\Template;
use Contao\FilesModel;
use Contao\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
* @FrontendModule("logo",
*   category="miscellaneous"
* )
*/
class LogoController extends AbstractFrontendModuleController
{
    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        $image = FilesModel::findByUuid($model->logoSRC);

        Controller::addImageToTemplate($template, [
          'singleSRC' => $image->path,
          'size' => null,
        ], null, null, $image);

        return $template->getResponse();
    }
}
