<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'addLink';

$GLOBALS['TL_DCA']['tl_module']['palettes']['logo'] =
    '{title_legend},name,type;'
    . '{logo_legend},logoSRC;'
    . '{link_legend},addLink;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
;

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['addLink'] = 'logoLink, logoTarget';

$GLOBALS['TL_DCA']['tl_module']['fields']['logoSRC'] = array
(
  'exclude'                 => true,
  'inputType'               => 'fileTree',
  'eval'                    => array('fieldType'=>'radio', 'filesOnly'=>true, 'mandatory'=>true, 'tl_class'=>'clr'),
  'sql'                     => "binary(16) NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['logoLink'] = array
(
  'exclude'                 => true,
  'search'                  => true,
  'inputType'               => 'text',
  'eval'                    => array('rgxp'=>'url', 'decodeEntities'=>true, 'maxlength'=>255, 'dcaPicker'=>true, 'addWizardClass'=>false, 'tl_class'=>'w50', 'mandatory'=>'true'),
  'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['logoTarget'] = array
(
  'exclude'                 => true,
  'inputType'               => 'checkbox',
  'eval'                    => array('tl_class'=>'w50 m12'),
  'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['addLink'] = array
(
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('submitOnChange'=>true),
	'sql'                     => "char(1) NOT NULL default ''"
);
