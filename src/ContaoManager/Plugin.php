<?php

namespace designerei\ContaoLogoBundle\ContaoManager;

use designerei\ContaoLogoBundle\ContaoLogoBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoLogoBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
